---
title: Design for the new - Interventions
period: 9 May 2019
date: 2019-01-24 12:00:00
term: 3
published: True
---
After having mapped the practice behind our final intervention as it is today and as it will be in the future, our task for the Design for the New class is to define the **transition's pathway**, the steps we have to take to get to the desired future.

This is the core idea of transition design, which *"Instead of thinking in terms of one-off solutions, that are completed within relatively short time frames, thinks in terms of designing interventions that are implemented at multiple levels of scale, over short, mid and long time horizons." (Holon 2019)*

![]({{site.baseurl}}/transition.png)

Transitioning a practice means identifying which leverage points should be touched for the desired future practice to unfold. This means thinking at possible evolutions of the practice and identifying core shifts in terms of:

- Skills: emerging mental and physical routines to embed in the practice of the future
- Stuff: objects which could emerge as crucial in the practice of the future
- Images: new symbols to give meaning to the practice
- Links: the fleeting connections between the three elements, which contribute in defining what the practice is made of

**Short term - Experimenting with collective health in communities**

At this stage, the practice of understanding health and wellbeing starts to shift. I imagine to provide the tools and methodologies for people to start to be aware that health could be approached from a different perspective, the collective one.
At this stage people begin to explore the concept of colective, using sensors and other emergent technologies to diagnose systemic illnesses into their communities. Practitioners from healthcare, the makers world and designers join the communities in exploring the urban exposome.

- skills: community building
- Stuff: sensors, communities, data frameworks, collective illnesses
- Images: collective health, collective actions, gathering together

**Mid term - Welcome to the office of the Urban Doctor**

At this stage, collective health is being piloted at the institutional level.  Urban Doctors work in the public office, and provide a constant monitoring over the urban exposome. They take care of our urban collective illnesses by designing actions to eradicate them.
As individuals, we grow aware that much of what influences us as individuals influences us as a collective in the same way.

- skills: hybridization, data understanding, machine learning
- Stuff: systemic medicines, systemic illnesses
- Images: collective experiences

**Long term - Feeling the collective**

At this stage, we finally have the knowledge and tools to understand ourselves collectively rather than as individuals. We understand that we exist as an interconnected system, and are able to connect with this knowledge. Our health and wellbeing are not anymore an individual problem, but an issue tackled at the levl of our communities and urban ecosystems.

- skills: collective empathy
- Stuff: electronic organs
- Images: collective consciousness

This exercise helps me figuring out even more the path that my project should take.
As I said previously, the biggest effort I have to do regards a **shift in the mindset** of the urban collectivity for my project to happen. The more I think about it, the more I believe I have to translate the experiments I've been doing to explore my idea into **a methodology**.
Parallel to this course, I have developed an intervention in the IAAC building, measuring the exposures affecting us througout our daily lives in the building. The IAAC building is used in this case as a metaphor for the urban environment, and the aim of the experiment is to **explore and diagnose** collective illnesses that affect our urban systems.

The methodology I would like to come up aims at **involving more people into the discourse around collective health**, and let them experiment by themselves (individually or as communities) around the idea of the urban exposome, diagnosing collective illnesses and expanding their catalogue even more.
