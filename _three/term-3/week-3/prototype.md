---
title: Design for the new - Prototyping
period: 17 May 2019
date: 2019-01-24 12:00:00
term: 3
published: True
---

Which interventions can we do to transform the practice I've been exploring and bring it closer to the practice I would like to create with my project?

As I said before, the exercises for this class made me realize that the first action I should perform to bring my project closer to its final idea is to **develop a methodology to engage with the concept of collective health** through the urban exposome and the collective illnesses I diagnosed.

I missed the last class of the course, therefore, my exercise for the prototyping step will be to create this methodology, and use it as the final step of my year long intervention/exploration on collective health.

## Skills, images and the link between them
In terms of the framework we have been using, creating a methodology means intervening both on the images and the skills that are involved in the practice of understanding our health and wellbeing. Creating a methodology also mean intervening on the links between these two elements, because through the development of new skills I will make it possible to *understand* images differently, making it possible to act on them in new ways.

## Prototyping the methodology

The trickiest part of prototyping this methodology is for me to frame it inside the bigger picture of my project.
Over the course of the year, I researched a body of knowledge that is difficult to translate into a simple experience with a defined goal.


**The goal**
The goal of developing a methodology is for everybody **to be able to start exploring around the concept of collective health**.

This activity will hopefully create more awareness around the subject of systemic health, and will raise interest from the different disciplines I aim to engage in the process of creation of Doctors of the Future.

**The activities**

The methodology will allow users to perform two different activities.

*Diagnosing the illnesses of the future*
The first one is to diagnose within their community or space one of the four systemic illnesses that I designed as a result of my experimentations

Participants will be able to take part in the diagnosing process - an exercise to play the  part of a doctor of the future - with a minimal technological effort.

The illnesses will be diagnosed through a set of three questions, which the participant will answer to by performing only one action - putting a sticker on a diagram

The diagrams will be made up of four quadrants, one of them corresponding to the illness' spectrum.

By collecting answers from participants, the organizers will be able to diagnose their systems and the people inhabiting them as well

*Expanding the catalogue of illnesses of the future*
The second activity will be to expand the catalogue of illnesses of the future.

This activity will be carried on repeating the same experiment as I did:

- Mapping the exposures and defining site-specific indicators
- Defining a method to monitor these exposures
- Analyzing the data, looking for interconnections and then putting them into context to see if they qualify as illnesses
