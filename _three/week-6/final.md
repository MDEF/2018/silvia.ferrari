---
title: Final Design Studio
period: 17 May 2019
date: 2019-01-24 12:00:00
term: 3
published: True
---
*The full article on Doctors of the Future is available [here]({{site.baseurl}}/Doctors of the future (1).pdf))*

New technologies make it possible for us to access directly a whole new range of information about ourselves and our urban environment, and the amount of available information on ourselves and the city context we live in is growing in the form of data: we have health and lifestyle data about ourselves, our DNA could be coded, we can sense and monitor the wellbeing of the city as well. However, finding ways to connect this information and highlight the interdependencies and relations between us and what we are surrounded by is still a challenge. Experimentation across the fields of urbanism, healthcare and computer sciences have shown that there is a strong link between the way cities are designed and obesity, as well as patterns of mobility and the spreading of illnesses.
Further exploration across the boundaries of different practices could contribute to innovate the way we look at our environment and highlight the connection we share with it.

**Doctors of the future** is an intervention exploring how the urban context we live in impacts on our health and well-being. The aim of the intervention is to improve our life conditions by exploiting technology, intervening on our context, meaning the connection we share with our ecosystem.

More in detail, the intervention aims at:

## Speculating on the future of doctors and cities
Climate change, new technologies and emergent trends all contribute in defining a near future where we will need to look at our environment from a systemic perspective. The urban doctor will be a specific kind of doctor whose job will be to take care of the context we live in, looking at interconnection we share and its impact on our health and wellbeing. The speculation aims at imagining  a future where “ if social science could become more embedded in daily life then society could itself become more of a lab and more citizens could become part-time social scientists. Here we see a possible future in which the role of the specialist mutates into more of a coach and a partner, an aide to an intelligent society more than a caste apart”[9]

Actions:
Design a speculative scenario for 2050, imagining how this new profession will develop and how it will impact on future’s living. The aim of the activity is to use the scenario as the endpoint reference of my project: by defining the ultimate outcome of my explorations, I will be able to define a strategic path of intervention to get from the present to my desired future.

Output:
A speculative documentary set in 2050, describing the work of the Urban Doctor and showing how the perception of health and the practice of medicine has changed with the support of emerging technologies.

<iframe src="https://player.vimeo.com/video/344143454" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


## Research systemic health from a design perspective
Cities are complex systems whose infrastructural, cultural, economic and social components are strongly interrelated and therefore difficult to understand in isolation. The interrelation of the urban dynamic makes it necessary to approach the study of its impact on people from a systemic perspective, one that takes into consideration all the environmental and social determinants of health and wellbeing.

Actions:
Design a framework to be able to observe and assess the systemic interconnections taking inspiration from the medical concept of exposome - the total amount of exposures we experience in our life. The aim of the activity is to use data as the language of interactions and isolate relevant interconnections affecting our health and wellbeing
Execute an experiment at a small scale: instead of a city, I will use IAAC building as the context in which we live. The aim of the experiment is to assess and map the exposures and define how their interconnection affect us.

Output:
The definition of a set of illnesses of the future, collective manifestations of decreasing health and wellbeing which can be detected by the interconnection of multiple exposures and measured through data gathering.

![]({{site.baseurl}}/illnesses.png)

## Design tools to transform data into information
We already have the tools and the knowledge to gather health-related data from ourselves and the environment. However, these data alone have little to no meaning for us. Emerging trends in the field, such as self- tracking, allow us to gather information on ourselves and our patterns of living towards defined goals, but they fail in letting us understand ourselves as part of a more dynamic collective. On the other side, environmental monitoring alone is inefficient in providing us with a complete and meaningful understanding of how our living is affected by external factors

Action:
Design tools for the doctors of the future:  once having isolated the relevant interconnections, I will define a set of illnesses of the future- systemic and interconnected exposures affecting our individual and collective health.

Output:
The first tool to be designed will be a methodology to contribute to the project, diagnosing. In the final exhibition, people will be able to self diagnose the illnesses of the future by sharing data through the Domestic Data Streamers’ methodology.

<iframe style="border: 1px solid #777;" src="https://indd.adobe.com/embed/f396fa58-afed-46bc-a6a7-ded15c8a165a?startpage=1&allowFullscreen=true" width="525px" height="371px" frameborder="0" allowfullscreen=""></iframe>

The goal of developing a methodology is for everybody to be able to start exploring around the concept of collective health.

This activity will hopefully create more awareness around the subject of systemic health, and will raise interest from the different disciplines I aim to engage in the process of creation of Doctors of the Future.

The methodology allows users to perform two different activities.

*Diagnosing the illnesses of the future*
The first one is to diagnose within their community or space one of the four systemic illnesses that I designed as a result of my experimentations

Participants will be able to take part in the diagnosing process - an exercise to play the  part of a doctor of the future - with a minimal technological effort.

The illnesses will be diagnosed through a set of three questions, which the participant will answer to by performing only one action - putting a sticker on a diagram

The diagrams will be made up of four quadrants, one of them corresponding to the illness' spectrum.

By collecting answers from participants, the organizers will be able to diagnose their systems and the people inhabiting them as well

*Expanding the catalogue of illnesses of the future*
The second activity will be to expand the catalogue of illnesses of the future.

This activity will be carried on repeating the same experiment as I did:

- Mapping the exposures and defining site-specific indicators
- Defining a method to monitor these exposures
- Analyzing the data, looking for interconnections and then putting them into context to see if they qualify as illnesses
