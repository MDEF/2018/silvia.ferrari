---
title: Life after AI and the end of work
period: 28 March 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---

The idea of having a job is a very recent invention of our society.

What is a job?

- How the economy produces goods
- How to earn an income
- The mean by which people develop purpose and identity

In our system, jobs produce economic value. Traditionally, there are different ways of producing it:

1. labour - tasks that produce value
2. capital - assets that produce value

Lately, things are changing:

Firstly, **the value of labour is decreasing while the capital is growing** - a symptom of that is how people started buying houses just to store value somewhere. This drives a huge inequality crisis, because the value of labour can increase to a certain point but the value of capital has no limitation.

Secondly, **a new form of labour is emerging through the gig economy**, where the relationship between a contractor and an employee is informal: it's easy to get into the system and schedules are flexible. On the other side, there are less social protections to the job.

Thirdly, **the robots and the new structure of labour that comes with them**. In Amazon's warehouses, robots organizes themselves. The first consequence of automation is a different structure of the salaries and the rise in inequality that comes with it: the designers of the robots are well paid, the robot's maintainers are equally well paid but the humans working with the robots are poorly paid. The second consequence is the commodization of humans - amazon supermarkets have seamless experiences but we are also constantly monitored while producing data and creating patterns of experience, for example on the products we are about to pick and then choose not to

How does automation impacts on the economy and the future of employment?
This is already an existing issue. For example, the most common job per state in US in 2014 was the truck driver. Automation is going to impact hugely in that sector, leaving thousands of people - mostly close to retirement-without a job. but how is it possible to tackle this issue?
AI is going to impact hugely on employment, more in some jobs, less on others. Nurses and generally jobs that require empathy will be more difficult to be replaced by machines.

In the 1930s, JM Keynes already predicted in his book Economic possibilities for our grandchildren that at some point work would have become scarce and we would have worked less. Interestingly,  Keynes was also a supporter of balance in the market, as opposed to eternal growth. Because of that, he was against the concept of debt as an economic tool. He argued that interests on debt make it possible to create the illusion of value, by associating an amount of money to something that does not exists. Eternal growth is created by allowing speculation on that amount of money, but the whole system is based on something that does not exist, and is bound to fail and recreate itself periodically, creating inequalities in the process. He tried to make his point at the end of both World Wars, but failed. After the end of the First World War, he participated in the Peace Congress in Paris as a member of the British delegation. There, he advocated for lighter war sanctions on the Germans, arguing that leaving them with a huge debt to pay would have favored inflation, leaving the country unable to thrive and the whole continent unbalanced in terms of economic power. After the end of the Second World War, he was invited to the Bretton Woods conference, where the Allies discussed how to regulate international commerce and the payment of the debts for reconstruction in Europe. There, again, he supported the creation of an international clearing union which should have made it impossible to speculate on countries' debt, but he failed again and the International Monetary Fund was created instead.
The economic system that was shaped through historical decisions now requires for us to work the way we are accustomed to, but is it necessary? What if we imagine a different work life balance?
A different way is already manifest: while the institution of work is being reconsidered generally, creative professionals already live following a different routine, where weekends and work hours are more flexible than before.
Can the rise of automation bring a positive change for us?
It is true that machines will disrupt the job market, but why are we so worried by the possibility of robots taking our jobs?
Our society make us feel useless without a job, and we also have to provide for ourselves through the economic compensation coming from it, but what could happen if we had a Universal Basic Income?  What will people do if this scenario becomes reality? It could become an age of endless creativity, a return of craftmanship. In such a scenario, the economy of care could become more relevant, because having a job would be more and more a matter of purpose, of finding a meaning in life outside the boundary of a standard 8-5 job.

There are huge issues underlying the rise of AI and the future of jobs, such as the **commodization of humans**. Such an economy would be supported by an endless flow of data transactions, transferring data from our automated slaves to companies that provide us with services Also, the **quality of automated decisions** is going to become an increasingly important factor for future development of AI:  the decisions machines can take are as good as the process built to get the machine to decide. Their quality depends on the quality of the process, the care put in making them as good and not biased as possible. Bias are embedded in that process and impact on everything that follows.

## The transitioner

As an exercise for this class we imagined a new job, **the transitioner**: the person who help you navigating life in a world where you don't have to be employed but you still need to be doing meaningful tasks

We imagined that in year 2050 jobs as we know them will be automated. People won't have a career for their whole life, the whole system will be more liquid than now. We will be able to move from one job to the other following our interests and being able to build skills in a non linear way thoughout our life. We imagined that in such a scenario people would need a guide to navigate the system, someone who would be able to match skill, interests with existing needs in the market. That job would be the transitioner's job, a goverment officer helping people to find purpose in their lives. Instead of a salary, the transitioner would get an increase in their Universal Basic Income. We imagined a Linkedin ad of 2050, where the whole description of the job is detailed. On the same page, we imagined ads for other jobs which were inspired by our final project's work

![]({{site.baseurl}}/Linkedin.png)

## Relevance for my project
- Imagining jobs of the future, where AI will disrupt the system we live in
