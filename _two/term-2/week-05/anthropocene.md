---
title: Designing for the anthropocene
period: 15 March 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---

Around 12000 years ago humans started interacting with nature and changing its course by blending species, contributing in deciding which were to disappear, which had to thrive, which to be modified by combination. These modifications were at a small scale until 150 years ago, when we started taking part in the **redesigning of the planet at a global scale**: following the industrial revolution, we started polluting air, land and water with the extractions and exalations from coal mining, influencing a geological redesign of our planet

Can we say we have contributed in modifying the planet so much we are now entering a new geological era? It could be argued so: Anthropocene is a geological era defined by the fact that humans are changing multiple planetary variables at the same time. There are many indicators we could use to detect our interventions, for example the increased level of radiation after the invention and test of the atomic bomb or the layer of chicken bones, coca cola cans and plastic that we created in our terrain.  

The concept of **great acceleration** explains the becoming of Anthropocene: in a few years, many environmental and economical variables changed at the same time. Even though we are now able to influence the environment on a global scale, our power of intervention is not infinite: there's plenty of indicators to prove that, and we are flooded by dramatic statistics of how we are slowly destroying our habitats. For example, out of all the CO2 in the planet 63% has been produced after 1992.

## The scale

We cannot understand the extent of the environmental situation we've contributed in creating.
We are clueless about the scale, impact and future development of climate change. We know that animals are disappearing at an increased pace, and insects as well. Environmental instability might trigger very unpredictable reactions.
The impact of Anthropocene is starting to show in politics as well. Climate refugees are not recognized by the international law but exist: populations have already started to suffer from environmental disasters and the consequences of climate change, which has forced them to leave their homes in search of new places to live.

## A plan

Strategies can be put in place to tackle the issue. The Paris Agreement defines common broad goals to tackle climate change: the countries involved pledge to work to limit the rise in the Earth's temperature to 2C in 2100, a step that will require a radical reduction in the use of the fossil fuels that are the primary cause of global warming. Emissions will have to be reduced to 0 between 2050/2100. Reforestation, geoengineered soulutions and the implementation of carbon sequestration technologies could help meeting the Paris Agreement's standards, but the core issue is to reinvent the system, making it possible for us to thrive without being such a planetary burden. As we discussed in our [Material Driven Design course](https://mdef.gitlab.io/silvia.ferrari/two/material-driven-design/), sustainability has to be supported by a system that values it, putting it at the core of innovation. It is necessary to shift our perspective, and become aware of the fact that we are not the only affected species, we're not superior. It is arguable that we are not even separate from other entities and living things on the planet.

The Paris Agreement is the first international agreement introducing a long term policy to protect the future of the planet:
it never happened before to take a political decision on something that is going to happen 808 years from now.
Does this opens the door for a new concept of political rights of the future? How can we put a long term perspective at the core of environmental policy?

## The Albert institute



<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTo3Idqj3oDL6YSKx3xQ7aX2BVtR3NuHKXFNFDZsDDg_-EZXoIH7L5tmiBTIvuVcDf8ECGxsO4cVRvz/embed?start=false&loop=false&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

In environmental matters, thinking long term empowers people who do not vote but have rights.
Why is it so difficult to adopt this point of view when tackling climate change? Why do we keep focusing on short term solutions?
The answer lies in the nature of the decisions that are made in this matter: environmental matters are driven by national and international policy.
Policies are influenced by immediate problems such as money - the costs of the actions to perform, external pressures in the form of corporate lobbying and the immediate interests of politicians, who may use votes and decisions to increase their power.

How is it possible to override these problem and enforce an intergenerational contract to protect the environment?
Our team created **Albert**, an AI powered index to forecast future (50 years) environmental impact of policies. It is [proven](http://superflux.in/index.php/work/futureenergylab/#)that showing long term consequences of environmental behaviors is a powerful way to enforce a long term perspective, but how do we make it a constant part of the discussion?

The index is inspired by existing financial and insurance policy algorithms, which forecasts future possible risks. More in depth, Albert uses a method already used in trading called “Genetic Algorithms”. Compared to the known Neural Networks, these algorithms use the concepts of natural selection to determine the best solution for a problem. As a result, GAs are commonly used as optimizers that adjust parameters to minimize or maximize some feedback measure, which can then be used independently or in the construction of a Neural Network.

As we imagined it, the algorithm's definition would be the result of a broad political process, a democratic effort to create a new governmental tool. Bias are unavoidable in the creation of rules for society, but a large participation in the decision-making process ensures agreement through mediation. This is what happened when shaping modern democratic constitutions: we imagine that the structure of an AI powered governmental tool should follow the same rules, avoiding this way the ethical issues that so many corporate-designed algorithms suffer.

Albert would not directly make policy: it would serve as an indicator, a tool to start a discussion on the environmental impact of national/international decisions. It would be part of the political discussion and could be used by opponents/backers to the proposed policy as a tool to engage over long term issues. Its rating would be based on the circular economy's definition of sustainability: sustainability is achieved when an action has a positive impact for the planet - meaning that is bearable, the people - meaning that the action is equitable, and the profits - meaning that it it viable and doable from a financial standpoint.

## Relevance for my project
- Anthropocene's consequences on health
- AI building: avoid bias by expanding the discussion


## Readings

- [THE FUTURE ENERGY LAB](http://superflux.in/index.php/work/futureenergylab/#)
- [The future is now](https://media.ifrc.org/innovation/future-and-foresight/the-future-is-now/)
- [Humanitarian futures](http://superflux.in/index.php/work/humanitarianfutures/#)
- [Sustainability and Circular Economy](http://ec.europa.eu/growth/industry/sustainability_en)
