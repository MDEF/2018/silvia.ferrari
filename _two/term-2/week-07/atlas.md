---
title: The Atlas of weak signals
period: 8 March - 4 April 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---

In foresighting, a weak signal is defined as a presage of a strategic discontinuity and it differs from trends, which are already visible tendencies, and wild cards, which are unpredictable and highly impactful events.
The atlas of weak signal class was about discussing and building scenarios around early signs of current small changes.
Throughout the course, we discussed and elaborated scenarios on 25 weak signals, clustered in 5 macro-topics: surveillance capitalism, Anthropocene, automation, end of the nation-state and future gender issues.


## Principles of intervention

I enjoyed very much building weekly scenarios. Even if sometimes it wasn't directly an exercise on my project, speculating on the future required to think about the interconnection between issues.  
**Looking at the scenarios I've participated in designing, I realize I can imagine a future where all of them happen at the same time. To me, this is a signal: whatever I discussed with my classmates in the past five weeks, I have the feeling that it was a discussion about my principles**, and how I would like present issues to evolve in the future.


In a broad sense, making sense of this complexity gave me direction and purpose in my action. As I wrote before, in my [Introduction to future's submission](https://mdef.gitlab.io/silvia.ferrari/one/a-introduction-to-futures/), I think we live in a time where we mostly feel lost about what's happening to the world. The dichotomies we have used for a long time to define the purpose of our actions are not anymore a valid instrument to evaluate present and future issues. Chaos, complexity and contradictions permeate our systems in every possible aspect, and politics have failed to make sense of these interdependent relationships and complex networks. I believe much of the political mess happening in Europe and the US has to do with people finding simple answers to complex issues in the right-wing, populist movements. It's a shift in principles and beliefs driven by a major fear of the unknown, and I have been struggling to look for a way to make sense of it myself.
For me, in the end, **the whole exercise of scenario building was about organizing my thoughts on complexity and defining my principles for future actions and interventions**.

**Data ownership**

In the age of surveillance capitalism, our reality is increasingly becoming a commodity through the exploitation of our patterns of life for data gathering. The dynamic of this system we live in requires for us to be engaged online as much as possible because the data we produce is the source of information on which services are provided. Our relationship with the online life changed massively through the smartphones, which brought us closer to information: from the perspective of the attention economy, every time we are using our phone is an opportunity to mine data and to understand the patterns of behaviours that are so important to create and market services. Our attention is so valuable that [boredom could be considered as a thing of the past](https://onezero.medium.com/the-death-of-boredom-3001a24dc709).

I don’t think that making sense of the attention economy starts with accepting or rejecting it, because it's simply the system we now live in. We should, however, stop being passive towards it, because manipulation could live as long as one is ignorant about it. **As data producers, we have a lack of ownership over the system and inadequate knowledge about the mechanisms of data extraction**. The limits on ownership of our data are so massive we don’t even notice them: for example, it is impossible to transfer playlists from Spotify to Apple music, but we are not inclined to question it. I also believe, [as said about China](https://mdef.gitlab.io/silvia.ferrari/two/on-china/), that much of what we perceive as manipulation could be also discussed as a matter of trust in the system: is it really a problem to be mapped by Google to understand traffic patterns if we trust in Google? I think it is a consequence of extreme capitalism to decide not to care for the mechanism that produces something we use, but at the same time, I believe that more transparency about the use of data and more control over storage would make us much more eager to share.
The scenario building I participated in aimed at designing a **circular economy for data** through a fictional **Data Funeral Home**. The starting point of the speculation was acknowledging that our data never dies, mainly because we’re not the ultimate owners of it, even though the concept of data cooperative is increasingly discussed. Could we imagine a near future where we are the ultimate owners of our data and their destiny? What would happen? In our speculation, specific companies -funeral homes for data - would provide the service, allowing to erase, sell or donate all or some of our data (for example health data donated for research)throughout our lives or at the time of our death. In this way, data could become part of a metabolic process where after being used, it could be reused or destroyed.

**Systemic interconnection**

We cannot understand the extent of the environmental situation we’ve contributed to creating. We are clueless about the scale, impact and future development of climate change. We know that animals are disappearing at an increased pace, and insects as well. Environmental instability might trigger very unpredictable reactions. We have contributed to modifying the planet so much that it could be argued we entered Anthropocene, a new geological era defined by the fact that humans are changing multiple planetary variables at the same time. There are geological indicators we could use to detect our interventions:  the increase of radiations after the invention and testing of the atomic bomb or the layer of chicken bones, coca cola cans and plastic that we created in our terrain. The impact of Anthropocene is starting to show in politics as well. Climate refugees are not recognized by the international law but exist: populations have already started to suffer from environmental disasters and the consequences of climate change, which has forced them to leave their homes in search of new places to live.

Strategies can be put in place to tackle the issue - The Paris Agreement defines common broad goals to tackle climate change - but **the core issue is to reinvent the system we live in, making it possible for us to thrive without being such a planetary burden**. As we discussed in our [Material Driven Design course](https://mdef.gitlab.io/silvia.ferrari/two/material-driven-design/), **sustainability has to be supported by a system that values it, putting it at the core of every innovation**. It is necessary to shift our perspective, and become aware of the fact that we are not the only species affected by climate change, we’re not superior.**We live in an interconnected balance with the rest of the planet**, it is arguable that we are not even separate from other entities and living things on it. Acknowledging the existence of this interconnection is necessary to shift the drivers of our human action.

The scenario building I participated in aimed at enforcing a long term vision about climate change in policymaking. Why do we keep focusing on short term solutions? Policies are influenced by immediate problems such as money - the costs of the actions to perform, external pressures in the form of corporate lobbying and the immediate interests of politicians, who may use votes and decisions to increase their power. Our team created Albert, an AI-powered index to forecast future (50 years) environmental impacts of policies. The index would not directly make policy: it would serve as an indicator, a tool to start a discussion on the environmental impact of national/international decisions. Its structure is inspired by existing financial and insurance policy algorithms, which forecasts future possible risks. As we imagined it, the algorithm’s definition would be the result of a broad political process, a democratic effort to create a new governmental tool. **Biases are unavoidable in the creation of rules for society, but large participation in the decision-making process ensures agreement through mediation.** This is what happened when shaping modern democratic constitutions: we imagine that the structure of an AI-powered governmental tool should follow the same rules, avoiding this way the ethical issues that so many corporate-designed algorithms suffer.

**A mindful automation**

In the near future, AI is going to impact hugely on employment, more in some jobs, less on others. Repetitive jobs will be probably more impacted than those - like nurses - that requires empathy.
Automation comes with huge underlying issues. First of all, such a system could imply the commoditization of humans. As the case of the Amazon supermarket's shows, our future lives could be a constant flow of seamless, pleasant experiences driven by the constant monitoring of our patterns of engagement. An automated economy would be supported by an endless flow of data transactions, transferring data from our automated devices to companies that provide us with services. Again, **data ownership and awareness over extraction mechanisms** emerge as relevant topics: it is our passiveness that doesn't allow to imagine a better future, not technology in itself.
Another emerging issue related to automation is the **quality of automated decisions **. The decisions' machines can take are as good as the process built to get the machine to decide. Biases are embedded in that process and impact on everything that follows. The quality of decisions depends on the design of that process. As I argued building a scenario for longterm thinking, **biases are unavoidable but participation in their design ensures mediation**.

As for the attention economy, I don't believe that making sense of the future of human labour in an automated world starts with accepting or rejecting it, because it's simply the system we now live in. However, **I think that we should start being mindful about machines: AI developments in terms of jobs and services have been mainly a corporate matter so far. There is a lack of other voices in the field, a lack of representation of interests others than those of the capitalistic market.** Institutions are way behind corporations in terms of technology and knowledge of the field. Filling this gap is a necessary task if we want to avoid the rise of a proper corporate governance, where private companies are the only legitimate providers of services and decisions are made following capitalistic principles.

For the scenario building, we imagined a different kind of 2050. In our idea, the government would have taken control over the job's automation and people would receive a universal basic income. In that world, people would not have a career for their whole life, the whole system would be liquid. Moving from one job to the other following personal interests, being able to build skills in a non-linear way, people would need a guide to navigate the system. The transitioner's job -a government job- would be to match people's skills and interests with existing needs in the market.

**Governing the complexity**

In our present time, we live with the consequences of [the progressive dying of modern ideologies: we are losing confidence in the values that built our western societies and states](https://www.economist.com/open-future/2018/10/08/are-liberals-and-populists-just-searching-for-a-new-master). The speed of information sharing in our systems has increased since the globalization of internet. News can escalate in relevance in a few hours, and the consequences of this escalations have unpredictable systemic impacts. The audience of any information has increased in size enormously, and this influences how facts are defined: different worldviews may clash in assessing information, and defining truths and knowledge is increasingly difficult. The more we know, the faster we change, as Yuval Harari said in his latest book Homo Deus: “The more we know, the less we can predict. This is the paradox of historical knowledge. Knowledge that does not change behavior is useless. But knowledge that changes behavior quickly loses its relevance. The more data we have and the better we understand history, the faster history alters its course, and the faster our knowledge becomes outdated”.

National governments are failing in tackling this complexity, and I believe signals of this are pretty clear:

- The globalization of information and financial flows makes it very difficult to govern change at the level of the single nations. Legislation in democracies takes time, and change now happens faster.

- For this reason, **we hold global private networks -such as Facebook- more accountable than our policy-makers over fundamental contemporary issues such as privacy and data ownership.**

- This rise of corporations as global actors creates huge **economical inequalities**: money flows into the financial market and is then privately redistributed, making **corporations the most powerful stakeholder in the institutional field**. Also, **states become poorer** through obsolete taxation systems that make it difficult to hold global corporations accountable for their profits.

- The struggle for dominance over technological innovation is defining a new polarized balance of global power between China, the US and Russia. This "war" involves a mix of both corporate and institutional powers fighting over of geopolitical, technological and economic issues that are difficult to understand together.

In this context, as a European, I believe it is quite telling that the most relevant piece of legislation that affected me in the past years is the European GDPR.
**It seems to me that continental legislature is able to move away from national political issues and holds the flexibility and audience needed to govern complexity. At the same time, movements of people, resources and innovation make the urban environment the most vital and interesting one to look at.** Decisions taken at the city level are easier to take and enforce, and it seems to me that the bond that keeps people together under an institution is stronger in the urban environment. The emergence of cities as new institutional units is already happening. In Italy for example, following the very recent approval of strict, right-wing national legislation forcing migrant's deportation, a number of cities decided simply not to apply the law.
Imagining the future of governance, I think we have to work towards a **more flexible system, one that is at the same time faster in absorbing change and more easy to access for people.** In this way, we could start holding institutions accountable again.

## Impact for my project

The class helped me find new angles to look at my project. In the end, I now find it more interesting and engaging than before. As I wrote [here](https://mdef.gitlab.io/silvia.ferrari/two/midtermreview/), I have been struggling this term with keeping the focus on my ideas. Grounding *Doctors of the future* in terms of principles and beliefs made working on it more interesting than before.

In terms of narrative, the class was a useful exercise: part of my project is building a scenario around how I believe the doctor's profession will change in the future. To do so, I took some of the weak signals discussed in class and researched them. More in detail, I looked closely at:

- **Future of jobs**: the doctor's profession is going to be affected a great deal by automation. Artificial Intelligence is already being used to diagnose patients, and it is considered to become more efficient than human in doing so. In such a scenario, AI presents an opportunity for doctors to focus on the relationship with the patients, and to approach health through a more holistic perspective

- **Longtermism**: Systemic thinking is at the core of longtermism in environmental issues. A more holistic perspective over the interconnections affecting us and our environment is becoming increasingly relevant: the researches on exposome and upstream medicine are an example of that.

- **Circular data economy**: I am very interested in possible frameworks that could ensure personal ownership of the data produced. When I first came up with the idea of my project, data sharing was one of the topics I was interested the most. Looking into a circular economy for data gave me the possibility to research more into data cooperatives and emergent trends in data ownership frameworks.

## A methodology for the  weak signals

Parallel to building scenarios, each week we mapped the weak signals, looking for keywords to connect them to one another and to build a shared repository of resources. The exercise was useful for my research into Doctors of the future, as it required me to keep a bibliography on the most relevant weak signals for my project. The process we followed tried to identify and organize signals by:

- **Scanning the internet** looking for weak signals. In this phase, we tried to look at fringes, secondary topics close to the main one we were researching on to identify anomalies, outlier data that could contribute in defining a specific pattern for the weak signal. We aimed at finding data hubs in relation to the identified weak signals. We did so by collecting links, websites and articles that we considered relevant for the topic.

- **Organizing the resources**: to extrapolate the data we found, we built a database with Google Sheets. Each tab of the sheet is devoted to a weak signal and the relevant links for each weak signal are organized by category. To connect the different weak signals we used data scraping: a Python script for sentiment analysis allowed us to collect keywords from each one of the links collected for every weak signal. Keywords were then grouped using Kumu, a network analysis tool that maps visually the connection between each keyword, weak signal and general topics we discussed.

## Readings - Weak signals for Doctors of the future

**Climate change**

* [The effects of climate change on human health – and what to do about them](http://scopeblog.stanford.edu/2017/05/09/the-effects-of-climate-change-on-human-health-and-what-to-do-about-them/)

* [Why we should care that climate change is making us sick](https://scopeblog.stanford.edu/2017/12/20/why-we-should-care-that-climate-change-is-making-us-sick/)

**AI and doctors**

* [Why AI will make healthcare personal](https://www.weforum.org/agenda/2019/03/why-ai-will-make-healthcare-personal/)
Assessing the impacts of AI in Healthcare: personalized medicine will spread, costs will be reduced

* [How Artificial Intelligence Can Make Doctors More Human](https://onezero.medium.com/how-artificial-intelligence-can-make-doctors-more-human-9424f5a8122e)

* [Your Future Doctor May Not be Human. This Is the Rise of AI in Medicine](https://futurism.com/ai-medicine-doctor)

* [Confronting Dr Robot Creating a people powered future for AI in health ](https://media.nesta.org.uk/documents/confronting_dr_robot.pdf)

* [AI can’t replace doctors. But it can make them better](https://www.technologyreview.com/s/612277/ai-cant-replace-doctors-but-it-can-make-them-better/)

* [Meet the GP of the future](https://www.telegraph.co.uk/wellbeing/future-health/the-doctor-of-the-future/)

* [Rethinking health innovation](https://www.ucl.ac.uk/bartlett/public-purpose/research/research-streams/rethinking-health-innovation)

* [Digital medicine: bad for our health?](https://www.ft.com/content/3ed1cc6c-0612-11e8-9650-9c0ad2d7c5b5)

**Patterns of urbanization**

* [Is Inclusive smarter than Smart?](https://medium.com/@UNDPEurasia/are-inclusive-cities-smarter-than-smart-13e2db4c407f)

* [Smart Cities: Ordinary Citizens Were The Missing Link All Along](https://towardsdatascience.com/smart-cities-ordinary-citizens-were-the-missing-link-all-along-7487e1753e10)

* [Barcelona Lab for Urban Environmental Justice and Sustainability](http://www.bcnuej.org/)

* [The urban’: a concept under stress in an interconnected world](http://theconversation.com/the-urban-a-concept-under-stress-in-an-interconnected-world-61000)

**Data as a tool for change**

* [Urban Data Pionieers](https://www.cityoftulsa.org/government/performance-strategy-and-innovation/urban-data-pioneers/)

* [IRYO](https://iryo.io/#intro)

* [Decibel.Live](https://www.decibel.live/)

* [Let’s make private data into a public good](https://www.technologyreview.com/s/611489/lets-make-private-data-into-a-public-good/)

* [ALGORITHMIC JUSTICE LEAGUE](https://www.ajlunited.org/)

* [Caring through Data: Attending to the Social and Emotional Experiences of Health Dataification](https://static1.squarespace.com/static/52842f7de4b0b6141ccb7766/t/5873a01137c581ab80198711/1483972626721/Kaziunas+et+al.+2017+Caring+Through+Data.pdf)

* [Self-Tracking for Health and the Quantified Self: Re-Articulating Autonomy, Solidarity, and Authenticity in an Age of Personalized Healthcare](https://www.researchgate.net/publication/301481639_Self-Tracking_for_Health_and_the_Quantified_Self_Re-Articulating_Autonomy_Solidarity_and_Authenticity_in_an_Age_of_Personalized_Healthcare)

* [When digital health meets digital capitalism, how many common goods are at stake?](https://www.researchgate.net/publication/329804081_When_digital_health_meets_digital_capitalism_how_many_common_goods_are_at_stake)

* [Harnessing the Power of Data in Health](https://med.stanford.edu/content/dam/sm/sm-news/documents/StanfordMedicineHealthTrendsWhitePaper2017.pdf)


**Urban wellbeing**

* [Cities and Mental Health](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5374256/)

* [Urban Emotions—Geo-Semantic Emotion Extraction from Technical Sensors, Human Sensors and Crowdsourced Data](https://www.researchgate.net/publication/268630662_Urban_Emotions-Geo-Semantic_Emotion_Extraction_from_Technical_Sensors_Human_Sensors_and_Crowdsourced_Data)

* [Health in Public Spaces: The challenge of inactive citizens for cities](https://urbact.eu/health-public-spaces-challenge-inactive-citizens-cities)

* [ECDC country visit to Italy to discuss antimicrobial resistance issues](https://ecdc.europa.eu/sites/portal/files/documents/AMR-country-visit-Italy.pdf)

* [City living marks the brain](https://www.nature.com/news/2011/110622/full/474429a.html)

* [MetaSUB: Metagenomics & Metadesign of Subways & Urban Biomes](http://metasub.org/)

A complete list of my project's readings is available [here](https://mdef.gitlab.io/silvia.ferrari/readings/)
