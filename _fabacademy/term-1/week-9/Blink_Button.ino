const int buttonPin = 3 ; // Here is where I set constants that does not change in the code


int buttonState = 0;  //This is a variable to read the button's status. It will change

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize  pin 7 as an output.
  pinMode(7, OUTPUT);
  // initialize button pin 3 as an input
  pinMode(buttonPin, INPUT);  
}

// the loop function runs over and over again forever
void loop() {
  
 buttonState=digitalRead(buttonPin);
  

  //first of all we need to read the button's value
 

  // check if the pushbutton is not pressed. If it is, the buttonState is HIGH:
  
  if (buttonState == HIGH) {
    // turn LED on:
  digitalWrite(7, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(7, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
  } else {
    // turn LED off:
    digitalWrite(7, LOW);
  }
}
  
