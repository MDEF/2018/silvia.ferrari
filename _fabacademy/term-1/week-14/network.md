---
title: Networking & Communication
period: 24 April  - 1 May 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---
The assignment for this week is to read a sensor and send the resulting data to a classmate. Me and [Kat](https://mdef.gitlab.io/katherine.vegas/reflections/fabacademy/) decide to send values received from a humidity and temperature sensor from my computer to hers .

To perform our project we need:
- 2 Arduino Uno boards + cables
- 1 breadboard
- 2 NRF24 transceiver
- Cables
- 1 DHT11 humidity and temperature sensors
- Arduino IDE installed on both computers


## Setting up the humidity sensor on Arduino

I will play the sender and Kat the receiver. For this reason, we start installing the DH11 humidity and temperature sensor on my computer. As I learn following [this very simple tutorial](http://www.circuitbasics.com/how-to-set-up-the-dht11-humidity-sensor-on-an-arduino/) *"The DHT11 detects water vapor by measuring the electrical resistance between two electrodes. The humidity sensing component is a moisture holding substrate with electrodes applied to the surface. When water vapor is absorbed by the substrate, ions are released by the substrate which increases the conductivity between the electrodes. The change in resistance between the two electrodes is proportional to the relative humidity. Higher relative humidity decreases the resistance between the electrodes, while lower relative humidity increases the resistance between the electrodes.
The DHT11 measures temperature with a surface mounted NTC temperature sensor (thermistor) built into the unit. "*

There are two kinds of the same sensors, one with four and one with three pins. We have the three-pins one

![]({{site.baseurl}}/sensor.jpg)

First of all we set up the cables following the scheme we find on the tutorial's page

![]({{site.baseurl}}/scheme1.jpg)

Secondly, we add the corresponding library in Arduino IDE by searching it through the library manager.

![]({{site.baseurl}}/adafruit.jpg)

Then we add the simple code we find on the tutorial, which is available [here]({{site.baseurl}}/sketch_jun05a.zip)

Running the code an error message pops up on Arduino IDE: this is because - as it is visible on the image above- we uploaded the wrong library, one that belongs to Adafruit and is not able to read our sensor.
So we go back to the tutorial and decide to download the library it offers, adding it to our software through *Sketck>add library from .zip*. This way the code runs perfectly. The library we used is available [here]({{site.baseurl}}/dhtlib.zip)

![]({{site.baseurl}}/dhtlib.jpg)

To check that the humidity and temperature sensor is working, we open the **serial monitor** on my computer and look at the values appearing on the screen. We also touch the sensor to see if there's any variation in the values and everything seems to work well.

![]({{site.baseurl}}/serial.jpg)

## Setting up the NRF24 transreceiver

NRF24 is a transceiver: this means that is able to both send and receive data from others. It can send and receive data at the same time because the circuits powering both operations are separated and run independently one from the other.
In terms of frequency, the transceiver works between 2.4GHz to 2.5GHz, which is a free frequency.
Since Wi-Fi works around the same frequency, I learn on the [HackMD class document](https://hackmd.io/@fablabbcn/B15x5dn9V?type=view)that it's better to use the transceiver at the frequencies around from 2501 to 2525 MHz.

To complete our projects, we have to use two of them, one for each of us.

The first step is to connect the board to Arduino. To do so, we follow the scheme provided to us in class

![]({{site.baseurl}}/scheme2.jpg)

After that, we download the NRF24 library for Arduino, which we find through the library manager

![]({{site.baseurl}}/24.jpg)

Before trying out our experiment with the sensors, we decide to run the basic code we find on the HackMD class document.

There are two codes, one for the sender (me) and one for the receiver(Kat). If the code works, the phrase "hola mundo" will appear on Kat's serial monitor.
The code for the sender is available [here]({{site.baseurl}}/sender.zip). We had issues with the receiver's code found on the HackMD class, so we browsed our classmate's websites for troubleshooting and used [Jess' version of the same code](https://mdef.gitlab.io/jessica.guy/fabacademy/networking_communications/)which worked perfectly fine.

hola mundo image

## Sending the humidity and temperature sensor's values

Since the sensor works and the two transceiver work as well, we  put everything together to complete our assignment.

From the hardware point of view, we simply connect the humidity sensor to the same Arduino we're using to control my transceiver, connecting it to **digital pin 7** as we did at the beginning.

![]({{site.baseurl}}/IMG_3743.jpg)

Other than that, we have to put together the code for my Arduino to read the temperature and humidity values and then send them to Kat's Arduino, reading values through the serial monitor.

The working sender's code is available[here]({{site.baseurl}}/networking.zip)

Other than organizing the contents that we already had, the process required:

- Harmonizing *loop(void)/void loop()* and *setup* syntax. Since there is no difference in the syntax between the two versions, I guess it's having both of the versions in one code that creates an error. After we harmonized using only *void setup()* and *void loop()* the red alert disappeared

- Understanding why we could send information from my computer to Kat's but not visualize it on the serial monitor (even if from the serial monitor's timestamp we could see something had been sent).
After some tryouts and some help we understood that the issue was with the way values were printed by my Arduino. We modified the sender code in the loop adding a couple of lines to make it possible for the value's numbers to be converted into a character string array. This wasn't necessary with the original sender code because we just stated to print "hola mundo" anyway.

~~~~
str=String(DHT.temperature);
str.toCharArray(data,16);
~~~~

From the receiver's side, we had to modify the code as well to let it:

- Receive information from my Arduino and the sensor. For this reason, we added a line in the setup to make the receiver to start listening for incoming message. We found the command in [this very useful GitHub repository](https://maniacbug.github.io/RF24/classRF24.html) about the NRF24 sensor:

~~~~
radio.startlistening();
~~~~

- Being able to print the information received. For this reason, we addedd two lines in the loop:

~~~~
radio.read(&data, sizeof(data));
serial.prinln(data);
~~~~
