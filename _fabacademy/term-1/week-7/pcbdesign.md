---
title: Electronics Design
period: 27 February - 5 March 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---
## Electronics design

This week assignment was:

- Redraw the echo hello-world board,
- Add (at least) a button and LED with its resistor
- Make the board and test it

## Drawing a PCB

To draw the hello-world board adding a button and **two leds** I used **Eagle**.
To learn the software I followed a Fab Lab class, where we also started designing the PCB together. After that, we still needed to add the 6-pin component and I wanted to add a second led, so I followed [this tutorial](http://archive.fabacademy.org/archives/2017/doc/electronics_design_eagle.html)

Between the class and the tutorial, I was able to design my board following these steps:

- Create a new Eagle project and open the *schematic* window
- Download the components fab library from [here](https://cba.mit.edu/calischs/libraries/blob/27fcf5b00271f3033248c3d714614e2b90541a71/eagle/fab.lbr)
 and add it through *library > manage library > available > browse > open*
- Add all the components using the *add part* button, starting from the **attiny44 ssu** and following with VCC, GND, avrispsmd
- Start connecting the components using *net*: click net and link pins with the corresponding components. This procedure can become pretty messy, so I started using another procedure, using the command *name*: *net > point to a blank space > name > insert the name of the component to connect*. Spelling is relevant in eagle, so it has to be exactly the component's name.

![]({{site.baseurl}}/net.png)

- Continue to connect the pins to the avrispsmd following the names on the board: 4 to reset, 7 to mosi, 8 to miso, 9 to sck
- Continue adding the necessary component: in class we added a crystal and two capacitors, while the tutorial I followed suggested to add a **resonator**. I went with the second option as it helped me managing the board's traces better. I also added two leds with resistors and a button.
- To connect components, sometimes moving them or rotating them helps the organization of the board: right-clicking on a component makes it move

![]({{site.baseurl}}/schematic.png)

Once all the components are in place and connected, I switched to the *board tab* in Eagle. This is where the PCB design actually takes place, following the rules and connections established in *schematic*.

Initally, the wiring was a mess, but I used the *Rastnest* command to optimize it.
Following the optimization, I started moving the components to the board space and draw the traces.
As I wanted to use the vynil cutter to create my board, I decided to work with bigger traces and I inputed the value **16** instead of the standard **8** that we should use when milling.

The tracing process is done using *route* command and is really long: the optimization of it takes some time. I think a huge help comes from looking at previous works on the same board, to have an idea about which way is better to connect everything and keeping the board of a manageable size.
Apart from the tutorial, I also looked up the suggested documentations for this week, especially [this one](http://archive.fabacademy.org/archives/2016/fablabshangai/students/80/week6.html).

Some rules have to be followed: capacitors for example have to stay close to the microcontroller. It is also possible to intersect two traces, but to do so we have to add **jumpers**, which are resistors without any value. To avoid potential errors, it is better to wire them in the right position also in schematic.

Once happy with the traces, it is possible to add also text to the board. Since I wanted to cut copper instead of milling I avoided it.

![]({{site.baseurl}}/trace.png)


The last step is to run a test to highlight potential errors with the *DRC* command.

![]({{site.baseurl}}/DRC.png)


I discovered that I made lots of mistakes with the traces, especially because I didn't connect them properly to components until the *X*

![]({{site.baseurl}}/errors.png)


Once all the errors are corrected, it is time to export the traces and the outline for the cut/milling. To do so:

- In *View > layers* hide all apart from **top** and **dimension**
- In *File > Export* choose *image*: resolution has to be **1000** and the **monochrome** box has to be selected. This export is going to be the **traces**
- Go back to layers and hide **top**, leaving only **dimension**
- Repeat the image export: this will be the outline.


## Cutting the PCB

I cut my PCB in copper with the Roland gx-24. The process is quite easy but the machine is very sensitive, so it's important to follow some rules:

- Adjust the machines' wheel to the size of the copper: the wheels needs to have enough grip for the machine to recognize and scan the piece
- Export the .png traces file to .jpeg
- Open the roland software on the computer. Here import the .jpeg traces file
- Resize the cutting surface. To do so the machine need to scan the copper through *cutting setup > properties > get from machine*
- Place the board on the cutting surface. To do so import the board traces' jpeg, then right click on it > properties - input the original size of the board (it is possible to get it from *Eagle > View > statistics*).
- In properties, also *extract the outlines and use them to place the board in the space (the jpeg can be deleted)*
- Cut. The process is very fast but it's important to have no bubbles on the copper/plastic sheet. Having them may result in the machine breaking important traces, as it happened to me the first time

![]({{site.baseurl}}/broken.jpg)


## Soldering

As I discovered, soldering is more difficult this way than it is with milling. The risk is to touch the plastic with the soldering gun and melt it, so it is very important to use a **low heated gun**.

## Update

Because soldering was so difficult with the vinyl cut board, I decided to mill the same board in the standard way.

I thought the process would have required a small time, but I ended up working for one day straight to complete the assignment.

First of all, I discovered that it was better to **modify the board's traces for milling**: I designed two of them inside the microcontroller's space, and the milling machine is not precise enough to cut without breaking them. Moving one of the two traces obviously required moving a lot of other components as well, and I ended up adding also a jumper.

![]({{site.baseurl}}/image_def.png)

Once having moved the components and checked that the traces were ok with the DRC command, I created the traces and outline files and prepared them for milling through FabModules.

Everything went fine, but **the milling process somehow didn't work**: I tried two times and each of them the board didn't stick to the machine's surface, breaking the traces and ruining the whole process.

Part of the issue was caused by **the tape** that I used, which wasn't strong enough. But with my third attempt I realized something else: I was keeping the **Z value very high** on the board, with a lot of the endmill sticking out of its socket. This created a lot of vibration on the endmill, that transferred to the board and made it unstable. I lowered the Z as much as possible and I managed to finally mill the board.

Lastly, I soldered the board and I also started enjoying it. Eduardo explained to me a technique to solder properly and, although I still need A LOT of practice, I am satisfied by my improvements

![]({{site.baseurl}}/result.jpg)


## Downloadable files

[Traces]({{site.baseurl}}/traces_def.png)

[Outline]({{site.baseurl}}/outline_def.png)

[Eagle - Board + Schematic]({{site.baseurl}}/board.zip)
