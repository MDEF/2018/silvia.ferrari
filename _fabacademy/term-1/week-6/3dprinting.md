---
title: 3D scanning and printing
period: 20 - 26 February 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---

## Assignments

The assignments for this week were:
- Design and 3D print an object (small, few cm) that could not be made subtractively.
- 3D scan an object (and optionally print it)

I am starting to enjoy 3D modeling a lot, so this week I:
- Practiced 3D scanning with a small table
- 3D scanned and printed a person - my boyfriend
- 3D printed a map of my favorite place in the world


## 3D Scanning

I found 3D scanning really easy. The process requires:

- A scanner - I used a Kinect camera available in the Fab Lab
- A scanning software - I used Skanect

The first thing I scanned was a small table, then I tried with a person and decided to scan my boyfriend. Firstly I plugged the Kinedct to my computer and opened Skanect.
The interface is really easy and guide you through the process:

- **Prepare:** here you choose the dimension of the scanned scene and the place to save your files
- **Record:** here you start scanning and see the outcome of it live. Looking at the texture of the scanned object you can see immediately if it's going well or not. It is really important to **move slow** and to **remember to scan the object also from its top and bottom**. To complete this task I got a really good advice from other Fab Academy students: we put the small table on a reversed rotating chair with a surface on top. This way, instead of moving around the objects I simply had to move the rotating surface.

![]({{site.baseurl}}/rotating.gif)

- **Reconstruct and process** are the phases were you can polish a scan: it is especially important to **fill the holes** in the mesh.
- **Share:** you can save an object either in .svg or .obj.

## 3D modeling from a scan

I didn't go further with the chair, but I decided to 3D print my boyfriend. To prepare the 3d printing file I used **Meshmixer**.

With this software, I :

- Cut some parts in excess from the scan using *Select > Discard*
- Polished the scan using the *Sculpt* tool, especially the *drag, flatten, inflate and pinch brushes*
- Made a solid out of the scan using *Edit > Make solid*

![]({{site.baseurl}}/meshmixer.png)

I saved the file as an .stl and opened Rhino. With Rhino I finalized the modeling: I added a base to the scan, created a mesh out of the object and exported it in .stl

![]({{site.baseurl}}/rhino.png)

## 3D modeling a map

I found online a really good tutorial explaining [how to print a map in 3d](https://www.youtube.com/watch?v=bSNy9iUqDbI). The week's assignment explicitly told to avoid subtractions, but I added this object on top of the rest of the work.

The process is really simple and requires:
- A bitmap image -a heatmap of the place to scan- which I dowloaded from http://terrain.party/ (as suggested in the tutorial). The heatmap serves as a guide to create the 3d shapes: the whiter a point is, the higher it will become in 3d. Shades make shapes.
- Blender
- Fusion 360/Rhino

First of all I downloaded the heatmap. I wanted to scan (this specific place)[https://goo.gl/maps/gohb1Q15aTQ2] in Greece and the surrounding area. It's where I go on holiday in the summer since I was born, it's my favorite place in the world.

![]({{site.baseurl}}/merged.png)

*The heatmap*

Once dowloaded the heatmap from Terrain Party, the first thing to do is to create a 3d shape out of it using Blender. Here, in an empty new file, I:
- Created a plane
- Added modifiers: first *subdivision > simple* and then *displace*. The first one is a managing tool for the plane that we'll use later. The second modifier allows to add an image to the plane
- Added the heatmap as an image and wait for the magic to happen: the image comes out in 3d, following the colors in the heatmap. The tutorial suggest to chose the *merged* bitmap from the folder I downloaded from terrain party.

![]({{site.baseurl}}/magic.png)

The image came out weird and completely out of size. To make it right I:
- Changed color to *linear*
- Modified the subdivision value of the plane: the standard is 6, I put it at 8.
- Changed the strenght of the 3D projection. To make it more real, I have to decrease it to 0.2 even if the tutorial suggest a max value of 0.35. I guess this depends on the fact that the area I chose is quite big (40X40 km), very hilly and surrounded by the sea.  


Once done, I exported it to Fusion 360 in .obj

![]({{site.baseurl}}/final.png)

 Here I completed a few necessary tasks:

- Imported the .obj file as a mesh trough *Insert > Insert Mesh*
- Created a form out of it trough *Create > Create form > Utilities > Convert from quad mesh to t spline*
- Created the base for the map by sketching it and then extruding
- Scaled the map to the dimension I wanted. The original file is huge.
- Prepared the 3D printing file through *Make > 3D print*. It took me some time to perform this task: I didn't realize it, but I sketched a plane two times and Fusion did not recognize the second one. To solve the problem I had to perform the process three times, but I ended up with a nice .stl file ready to print.


## 3D printing

I used two different 3D printers. With **Creality 3D** and black PLA filament I printed the map of Greece, while to print my boyfriend I used the **Ultimaker 2** with white glowing-in-the-dark PLA filament.
Both prints were easy to set up: the software in the 3d printing Fab lab's room lets you calculate the printing time and adjust the parameters accordingly (like the density of the filament and the thickness of the print's internal structure).


![]({{site.baseurl}}/prints.png)

## Downloadable files

[3D scanned chair](![]({{site.baseurl}}/chair.stl))

[3D printing-ready Michele](![]({{site.baseurl}}/mike.stl))

[3D printing-ready map of Greece](Greece.stl)
