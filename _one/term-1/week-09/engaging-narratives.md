---
title: 09. Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---

This week we received insights on storytelling techniques for ourselves and our project, that we then applied to the ideas we developed in the past two months.

We met two different people working with storytelling: Heather from Kickstarter and Bjarne from Ducklings, who both helped us in framing our ideas into a story and gave us useful tips on how to create impactful narratives for our projects.

## Why is storytelling important?

Stories have always been used in societies to pass on values and habits, using narratives as a way to teach ideas and moral behaviours without showing them. Mythologies are the oldest examples of stories created to define the rules and behaviours of a community, but we keep this mechanism alive in contemporary society as well: the entertainment business has made it possible to communicate values and ideas through movies. Walt Disney, for example, understood the power of myths and leveraged on that to create [engaging narratives able to convey meaningful messages](https://www.economist.com/leaders/2015/12/19/star-wars-disney-and-myth-making).   **Stories are a tool to connect people and transfer concepts.** In a way, they could be considered [design black boxes](http://127.0.0.1:4000/silvia.ferrari/reflections/navigating-uncertainity/), as they allow to transfer meanings by embedding them in artefacts. Bjarke from Ducklings called these kinds of stories *interconnected stories*: stories creating a **shared meaning around a concept without having to articulate it**.

Storytelling is embedded in every practice today, because of how powerful it is in conveying messages and emotions. **Stories serve as an inspiration to take new paths**, so much that now [new business ventures are being imagined by sci-fi writers](https://medium.com/s/thenewnew/nike-and-boeing-are-paying-sci-fi-writers-to-predict-their-futures-fdc4b6165fa4), and they are **helpful in engaging people around an idea or a concept.**

Engagement and inspiration are so important in promoting ideas in a compelling way that storytelling is the tool to use when looking for project funding in the form of crowdfunding.
Heather from Kickstarter gave us insights on how storytelling for project funding should be. She spoke about the platform she works for, but the concept could be applied to a broader definition of project funding. A project's presentation is very important: **from the title to the rewards offered, possible backers should be engaged in trusting the project and feeling the need to participate in the effort**.

Storytelling is very useful when trying to turning an idea into a concept: **using narrative techniques helps to focus and to refine the key messages that need to be conveyed.**

## Storytelling techniques

The most important work on storytelling structures is Vladimir Propp's [The Morphology of Fairytale](https://www.scribd.com/doc/146825827/Vladimir-Propp-The-Morphology-of-Fairytale-pdf). Analyzing traditional Russian tales, Propp's studies aimed at identifying elements of fairytales common to all traditional narratives. The study is complex and detailed, but its main elements are still very relevant for modern storytelling.

In a more simple way, both Heather and Bjarne showed us useful methodologies to build stories around our projects.

**How to communicate a project**

The project value proposition should include:
- Title ( should be unique, desirable, succinct, specific, memorable)
- Subtitle
- Main image/video

The project description should describe:
- Who you are
- What you plan to make
- Where the project comes from
The project description should distil the idea in three main aspects, and communicate possible scenarios emerging from those aspects

**How to tell a story**

Each story should have a recurring structure, including:
- A lead
- A turning point
- A climax
- An interpretation of the story

## This is Amelia

To explain the concept I want to work on in the next few months, I created a visual story.


<iframe style="width:750px; height:563px;" src="//e.issuu.com/embed.html#35847251/66166566" frameborder="1" allowfullscreen></iframe>



## Personal Storytelling

The days with Heather were particularly interesting because we discussed the issue of personal storytelling.
As creatives, we need ways of communicating our practice and products to a wider public. Social media channels are helpful to fulfil this task, but to use them wisely we need to plan our presence in terms of goals, audience, channels and messages. It's important also to set a nice tone to the messages, keeping always in mind that **one of the goals of promoting is to receive feedback from a wider audience.** Since I find it difficult to "advertise" myself, I took the week as an opportunity to learn how I can promote myself in a way that is meaningful to me. I asked Heather about it because one of the things that I find most difficult is to set a tone to self-promotion that doesn't sound false or annoying. The chat we had was brief but very useful: she made me realize that I struggle with the idea of having to promote myself as a whole. She explained that a good self-promotion does not necessarily have to do with me a person, but only with my work, with the outcomes I produce. A professional voice comes before a strategy, and it has to do with **values** and **passions**. It doesn’t have to communicate everything about me, but it needs to be a consistent voice about a **specific angle** of myself. ** Self-promoting creative works don't need to be thorough, but consistent in the message**.
In the future weeks, I will make an effort in looking at examples of creative communications that resonate with me.

### Readings on storytelling & the power of narratives

* [Star Wars, Disney and myth-making](https://www.economist.com/leaders/2015/12/19/star-wars-disney-and-myth-making)
* [Nike and Boeing Are Paying Sci-Fi Writers to Predict Their Futures](https://medium.com/s/thenewnew/nike-and-boeing-are-paying-sci-fi-writers-to-predict-their-futures-fdc4b6165fa4)
* [The Morphology of Fairytale](https://www.scribd.com/doc/146825827/Vladimir-Propp-The-Morphology-of-Fairytale-pdf)
