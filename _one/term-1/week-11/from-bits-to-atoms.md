---
title: 11. From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-16 12:00:00
term: 1
published: true
---

This week was an introduction to the work we will do next term during the Fab Academy.

In the first part of the week we have been presented with the structure of the Fab Academy, from the way our work will be assessed to the methodology we will  use to document our weekly projects.
We have been given access to past year's repositories, and I spent some time browsing through the projects to have a better sense of the ideas that have been developed by past students.
I am excited about this next challenge and at the same time a bit worried, because the skills required for the Fab Academy are those that I lack the most.
I know that I can be a fast learner and I hope that, even if i used Rhino only in the precourses, I will manage to keep the pace required. Time management will be crucial for me, and I will also have to pay attention at documenting everything that I do. We have been told that our documentation should include:
* The story of how we developed our ideas, including the failures
* Enough information on our projects for somebody to replicate it

The second part of the week was a Fab Academy sprint project. The challenge was to create something using **one input, one output and two digital fabrication processes**. Me and my team (Gabor, Julia, Saira, Ryota) decided to work with music. We brainstormed for some time, deciding to create something that would:
 * Make many different sounds
 * Mix those different sounds
 * Make music through movement
 * React to light or weight

Our concept was to have drawings (black or white lines) as an input and music as an output. We researched similar projects online, and also asked to the week's teachers how we could develop our idea. We documented our project in the same way we will have to do for the Fab Academy, and our project's development repository is available [here](https://mdef.gitlab.io/scribble-sound/).

I started feeling ill after our initial brainstorming and went home before the end of class on the first day. I ended up missing three days, and went back to school just in time for the final presentation.
Because of this, my contribution to the project was minimal. I helped out where I was needed, without being in charge of any of the streams we worked on during the week. My main contribution was in improving the structure of the machine box: after some initial trials, we agreed that the laser-cut box was too weak to bear the power of the motor and of the eight wheels we used. We had to adjust the box, making it more solid by by adding wood bridges and reinforcing the structure hosting the motor.

I had my final project's idea when I acknowledged that we are surrounded by information that we cannot read because we have not still designed the interfaces to do it. I realized that we could understand our health and wellbeing in a much more deeper way by having a more systemic approach to them. Ultimately, I would like to focus on the design of new interfaces that could help us understand our wellbeing by taking our environment into consideration.
For the Fab Academy, I would like to work around the concept of interface, developing objects that make it possible to understand data in a meaningful way, for example:
* sensing data and transforming the inputs I receive into objets
* create interactive objects that change behavior according to the flow of data they receive

Following the final week and the feedback I received on my idea, I started thinking about going beyond the idea of making data understandable: **what happen after we understand data?** Could we make **objects that trigger a reaction based on data as an input?** During the Christmas holidays, I will research on this concept, also taking inspiration from past Fab Academy projects
