---
title: 01. MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---

# 01. MDEF BOOTCAMP

 ![Talking Brains]({{site.baseurl}}/brain.gif)

 *Indissoluble @ Poble Nou*


## Exploring ecosystems

I experienced the week as an extensive exploration of the ecosystems we are part of, from the small scale to the big picture.
This exploration helped me realize how complex it is to define the elements composing the various ecosystems and to map the relationships happening inside them. This core complexity emerged very clearly through the interactions we had with:

- Other individuals
- Groups on various scales (the whole class, my group of interests)
- The space surrounding us, both in the class and in the neighbourhood
- The digital spaces we set up
- The stories and experiences of the people in Poble Nou


## Ecosystems and their complexity

The perpetually changing nature of our ecosystem and of the elements composing it has been central to my reflections: as individuals, we are part of a class, an institute and a neighbourhood and through our decisions and interactions we participate, in an always changing capacity, with what surrounds us.
As individuals, we formed groups of interests inside the class and as a class, we interacted with the other actors living in Poble Nou.
Moving from an ecosystem to the other, our personal reflections on the neighbourhood have been discussed in groups, and our individual experiences have blended with those of the others, defining our collective opinions on the experiences we had together.

**Because of this constant movement, every ecosystem is multi-layered, and we are part of many overlapping ecosystems at the same time.**
The class itself is layered: the term *hybrid profiles* has been used on various occasions to define the mix of mindset and skills we will be able to provide at the end of the course. In terms of skills and interests of us participants, there is a majority of designers and architects, but social sciences are represented as well. We are diverse also in terms of experiences and the purpose we had in joining IAAC.
When combined, these layers allowed me to have a better, more textured comprehension of ourselves, both as individuals and as an ecosystem.

## Complexity and awareness
I think this awareness of the complexity surrounding us is instrumental to understand the main themes of the master, and as such it has to be exercised constantly through interaction with other people and machines: from documenting to talking in small and bigger groups, we shape our reflections on what surrounds us and exercise our creativity. In this perspective, I see technology both as a part of our ecosystems and as a tool to support our permanent research for new solutions.
The day spent setting up our digital space made me realize how important it is to share a common understanding, a language of the complexity to be able to decode it together.
Another useful insight has emerged during Oscar Tomico’s talk: to exercise awareness in a process, it is important to have a phase when attention is given to as many aspects as possible, without focusing since the beginning on a specific target that could weaken our perceptions

## Impact on my work
Since I'm interested in the democratizing power of technology and its impact on local communities, I found the idea of many intertwined ecosystems relevant for my future investigations.
In a complex system made of different, mixing layers, what defines a community? How the definition of such communities impact on their participation to local social and political processes? In which ways can technology support local communities?
