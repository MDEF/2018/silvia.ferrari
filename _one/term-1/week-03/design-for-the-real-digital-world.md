---
title: 03. Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---

# 03. Design for the real digital world


### 0.Thoughts on circular economy & the FabLab space

This week we experimented first hand how a circular economy works:
Taking advantage of Barcelona's waste collection system, we took scrap material from the streets of Poblenou and then used these recycled items to create and produce designs for our classroom at IAAC.

Digital fabrication played a big role in our work: software and digital tools allow to model and produce design items very fast and very accurately

I realized that to make the circular economy work, we need spaces and instruments to make it possible. **It is not just a matter of imagining a different production and consumption system: to change it, we need tools that help us facilitate the process, change our everyday habits and allow everybody to take part in the change.** In this sense, **digital fabrication could be an empowering space**, because it allows everybody to easily produce items and distribute knowledge.

However, I believe that to make it possible for the FabLabs to shift from an experimental to a mainstream tool of change, some gaps have to be filled:

- **Digital knowledge:** there is a huge gap between the average digital knowledge people have and the skills that are required to use the digital fabrication software and tools.

- **Design knowledge:** to fabricate means to know the materials. For example, there is a great difference among kinds of wood and these differences influence the way wood is used in digital fabrication. While designers and FabLab experts are accustomed to this kind of issue, the majority of people are not.  

- **Costs:** the FabLab experience is still too expensive for the majority of people. The machines are expensive to buy/use and the knowledge to use them has to be acquired somehow, probably by paying educational courses.


### 1.Our goal
Divided into groups, we had to imagine how to organize and furnish room 201 for our work. Our interventions should fit into a circular process, so we needed to take into account our present needs as well as the afterlife of objects and spaces at the end of this year in our design.

### 2.Brainstorming
We started by discussing together how we move in our space. We found three main issues in room 201:

![]({{site.baseurl}}/room.png)

*the issues we found in room 201*

In the way the room was organized, interacting was difficult because of the columns at the centre of the space: when people talked, others cannot see them or hear them well. Space had to be reorganized taking into account other factors: light in the room is mainly artificial and the airflow is somehow difficult.

Looking at the map of the room, we recognized that the columns we previously saw as obstacles allowed us to divide the room into two main areas:

![]({{site.baseurl}}/spaces.jpg)
*Imagining a new organization for our space*

### 3.Formal space - Do we need a different table setting?

The formal space is a space to gather together for lectures and activities, as well as study in small groups 

We worked on the tables, imagining how they could be rearranged to be flexible enough for our needs, but in the end, we realized that changing their shape/adding new features would require too much effort and would not be sustainable at all.

![]({{site.baseurl}}/table201.png)
*Our brainstorming process for the tables*

### 4.Informal space - What do we need to own our room outside formal educational moments?

The informal space is a space to experience the room outside the more structured study moments, answering our daily needs and allowing us to experiment

After a brainstorm, we realized we wanted to work on a coffee counter and some shelves, which could help to organize the space without making it too structured

![]({{site.baseurl}}/concepts.png)
*Our brainstorming process on the coffee counter and the shelves*

### 5.From a coffee counter to the Coffee Lab

Our coffee counter is designed with a circular process in mind: the coffee/herbal tea waste we will produce by using it will be recycled to grow mushrooms. Researching online we found much information on the growing process and we were able to design the counter to fit a small mushroom farm.
**Our idea was to produce an item that is both useful in everyday life and a space to experiment with biology and Arduino.**

To produce the coffee lab we worked on three separate streams

![]({{site.baseurl}}/process.gif)

*CNC milling, making and the mushroom box*

- **CNC Milling:** part of our group modelled the Coffe Lab with Rhino and then produced the files through Rhino CAM to feed the CNC machine. The process included setting up a tolerance space (0.25 mm each side of our models) for the machine to cut our pieces in the correct size and adjusting the corners to the end mill diameter.

![]({{site.baseurl}}/CNC.png)
*CNC milling process*

- **Making:** we worked in the FabLab facilities to build the table and the mushroom box. We selected the scrap materials we found in Poblenou, we measured and dismantled them to fit our needs and we then produced the structure of the coffee lab by cutting metal and wood, screwing and sanding.

![]({{site.baseurl}}/making.png)
*Making process*

- **Mushroom box:** firstly we researched online to understand the features that our box should have to allow us to grow mushrooms. The structure of the box should be flexible, to answer to different needs in the two phases of mushroom growing. Humidity and light levels play a big factor in the process, but they have to be different from phase to phase. We decided to add through Arduino two sensors to the box to help us monitor the growing conditions.
We modelled the box according to our needs (total darkness and humidity in the first phase, moderate light and fresh air in the second phase) and we build it. The Arduino box was created using laser cutting.

![]({{site.baseurl}}/box.png)
*Building the mushroom box*

During the modelling process, we changed some features of the Coffee Lab to improve its solidity and usability:

- Vertical walls were added to the upper shelves
- Shelves plugins were modelled to allow us to change the design of the upper shelves if we need/want to do so in the future
- The lower shelf changed completely to adjust to the mushroom box need and to include an iron surface recycled from an ironing board

![]({{site.baseurl}}/table.jpeg)
*Finally, the Coffee Lab*

### Old an new skills: what I learned during the week

I really enjoyed the week and the opportunities it gave me to reflect on the skills I have and on those I want to improve:

- **Teamwork:** most of my previous working experience has been in a group setting. I am used to sharing responsibilities, trust in teammates and rely on a networked intelligence. I was happy to find that other people have the same attitude, and it made me think that this is the setting I want to have in my future working experiences as well.
- **Creativity:** the whole digital fabrication process is really new to me, as I do not have a designer background. I could not participate in every step of the process and consequently, I had to focus on exploring just some of the many creative skills I still have to learn: I decided to work mainly with making and I had the opportunity to learn more about the FabLab facilities and tools. Still, I feel I have to learn more about modelling, and I look forward to future opportunities.
- **Storytelling:** through my previous job and my studies I became very skilled in analytical thinking and researching information. For example, I am really good at making presentations, but I didn't think this ability could actually be useful outside of a consultancy job. Instead, I discovered that the ability to define relevant information and put them in the right order to tell a story can make a difference in a hybrid environment, where very different skills join together.
